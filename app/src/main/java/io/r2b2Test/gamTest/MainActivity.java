package io.r2b2Test.gamTest;


import com.google.android.ads.mediationtestsuite.MediationTestSuite;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;
import com.google.android.gms.ads.admanager.AdManagerAdView;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import androidx.annotation.NonNull;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import com.google.android.gms.ads.initialization.AdapterStatus;
import io.r2b2.AdMediationAdapter;
import io.r2b2.error.exception.R2b2Exception;
import io.r2b2.mediation.AdMediationAdapterListener;
import io.r2b2Test.gamTest.databinding.ActivityMainBinding;
import kotlin.Pair;

public class MainActivity extends AppCompatActivity {

    private TextView mLevelTextView;
    private ActivityMainBinding binding;
    private AdManagerAdView mAdView;
    private TextView log;

    private void log(String text){
        Log.d("MyApp", text);
        log.append(text + "\n");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        log = findViewById(R.id.log);
        log.setSingleLine(false);

        TextView version = findViewById(R.id.version);
        version.setText(BuildConfig.VERSION_NAME);

        List<String> testDeviceIds = Arrays.asList("9CDB7752BE093392B075D4F6150A3265", AdRequest.DEVICE_ID_EMULATOR);
        RequestConfiguration configuration =
                new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
        MobileAds.setRequestConfiguration(configuration);

       // MediationTestSuite.launchForAdManager(MainActivity.this);


        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                Map<String, AdapterStatus> statusMap = initializationStatus.getAdapterStatusMap();
                for (String adapterClass : statusMap.keySet()) {
                    AdapterStatus status = statusMap.get(adapterClass);
                    log(String.format(
                            "Adapter name: %s, Description: %s, Latency: %d",
                            adapterClass, status.getDescription(), status.getLatency()));
                }
            }
        });


        AdMediationAdapter.Companion.setListener(new AdMediationAdapterListener() {
            @Override
            public void onAdapterInitializedSuccessfully() {
                log("onAdapterInitializedSuccessfully");
            }

            @Override
            public void onBannerLoadRequested() {
                log("onBannerLoadRequested");

            }

            @Override
            public void onError(@NonNull R2b2Exception e) {
                log("onError");
            }

            @Override
            public void onBidRequest(@NonNull String s, @NonNull Iterable<Pair<String, String>> iterable, @Nullable String s1) {
                log("onBidRequest");
            }

            @Override
            public void onBidResponse(@NonNull Iterable<Pair<String, String>> iterable, @Nullable String s) {
                log("onBidResponse");
            }

            @Override
            public void onAdLoadedIntoView() {
                log("onAdLoadedIntoView");
            }

            @Override
            public void onAdDisplayed() {
                log("onAdDisplayed");
            }

            @Override
            public void onImpressionReportSent() {
                log("onImpressionReportSent");
            }
        });

        mAdView = findViewById(R.id.adView);
        AdManagerAdRequest adRequest = new AdManagerAdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    public void openDebugMenu(View view) {
        MobileAds.openDebugMenu(this, "/187332344/SDK_Test_300x250");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}